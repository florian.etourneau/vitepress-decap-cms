import { h } from 'vue'
import DefaultTheme from 'vitepress/theme'
import Title from './Title.vue'

export default {
  extends: DefaultTheme,
  Layout() {
    return h(DefaultTheme.Layout, null, {
      'doc-before': () => h(Title)
    })
  }
}