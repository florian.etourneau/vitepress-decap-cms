import { defineConfig } from 'vitepress'
import { getSidebar } from 'vitepress-plugin-auto-sidebar'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Vitepress Decap CMS",
  description: "An easy editable Vitepress site",
  base: '/vitepress-decap-cms/',
  outDir: "public",
  vite: {
    publicDir: "static"
  },
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Examples', link: '/docs/markdown-examples' }
    ],

    sidebar: getSidebar({ contentRoot: '/', contentDirs: ['docs'], collapsed: false }),
    
    editLink: {
      pattern: 'https://gitlab.com/florian.etourneau/vitepress-decap-cms/-/edit/main/:path'
    }
  }
})
